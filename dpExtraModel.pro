#-------------------------------------------------
#
# Project created by QtCreator 2013-10-19T06:03:34
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dpExtraModel
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dbmodelextra.cpp

HEADERS  += mainwindow.h \
    dbmodelextra.h

FORMS    += mainwindow.ui
