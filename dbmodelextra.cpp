#include "dbmodelextra.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <qmath.h>
#include <QTime>

const int RECORDCOUNT = 100000;

DbModelExtra::DbModelExtra(QString dbname, QObject *parent) :
    QSqlQueryModel(parent),
    m_Label(dbname)
{
        QTime t = QTime::currentTime();

    if (initializeMemoryDB(dbname))
        setQuery(QString("SELECT * FROM %1").arg(dbname),MemoryDB);

    qDebug() << t.elapsed() << " <<<< create models";  //TODO: remove or convert to logging
    t.restart();

    while (canFetchMore())
        fetchMore();

    qDebug() << t.elapsed() << " <<<< fetchMore";  //TODO: remove or convert to logging
    t.restart();

    qDebug() << index(RECORDCOUNT-1,3).data().toDouble() << " <<<< constructor value test";  //TODO: remove or convert to logging

    qDebug() << t.elapsed() << " <<<< calc all cells";  //TODO: remove or convert to logging

}

int DbModelExtra::columnCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : 4;
}

QVariant DbModelExtra::data(const QModelIndex &item, int role) const
{
    int row = item.row();

    if (!item.isValid()) return QVariant();
    switch(role)
    {
    case Qt::DisplayRole:
        switch(item.column())
        {
        case 0:
            return QSqlQueryModel::data(item);
            break;
        case 1:
            return QSqlQueryModel::data(item);
            break;
        case 2:
            return index(1+(row-1),0).data().toDouble() - index(row,1).data().toDouble();  //data(index(row,1),Qt::DisplayRole).toDouble();
            break;
        case 3:
            return qPow( index(row,2).data().toDouble(),1+Label().right(1).toInt());
            break;
        }
    default:
        return QVariant();
    }

}

bool DbModelExtra::initializeMemoryDB(QString dbname)
{
    MemoryDB = QSqlDatabase::addDatabase("QSQLITE",dbname);
    MemoryDB.setDatabaseName(":memory:");
//    MemoryDB.setDatabaseName(QString("../cent%1.sql").arg(dbname));
    bool ok = MemoryDB.open();
    if (!ok)
    {
        qDebug() << "FAIL: sqlite in :memory: .open()  - " << MemoryDB.lastError();
        return ok;
    }
    ok = createMemoryTable(dbname);
    ok = populateMemoryTables(dbname);
    return ok;
}

bool DbModelExtra::createMemoryTable(QString dbname)
{
    QSqlQuery createDataTable(MemoryDB);
    bool ok = createDataTable.exec(QString("CREATE TABLE %1 ( %2, %3)")
                                   .arg(dbname)
                                   .arg("d1 REAL")
                                   .arg("d2 REAL"));
    if (!ok)
    {
        qDebug() << "FAIL: sqlite in :memory: .createTables()  - " << createDataTable.lastError();
        return ok;
    }
    return ok;
}

bool DbModelExtra::populateMemoryTables(QString dbname)
{
    bool ok = false;
    for (int var = 0; var < RECORDCOUNT; ++var) {
        QString insert_str = QString("INSERT INTO %1 (d1, d2) VALUES (%2, %3) ").arg(dbname).arg(QString("%1").arg(var * dbname.right(1).toInt())).arg(var);
        QSqlQuery insert_qry(MemoryDB);
        ok = insert_qry.exec(insert_str);
        if (!ok)
        {
            qDebug() << "FAIL:  populateMemoryTables() - " << insert_qry.lastError();
            return ok;
        }
    }
    return ok;
}
