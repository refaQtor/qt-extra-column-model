#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTableView>
#include <QDebug>

const int TABLECOUNT = 4;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    dbm(new QList<DbModelExtra*>)
{
    ui->setupUi(this);

    for (int var = 0; var < TABLECOUNT; ++var) {
        dbm->append(new DbModelExtra(QString("tbl%1").arg(var),this));
    }

    for (int var = 0; var < dbm->count(); ++var) {
        addDataTab(dbm->at(var));
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_Quit_triggered()
{
    close();
}

void MainWindow::addDataTab(DbModelExtra* mdl)
{
    ui->tabWidget->setUpdatesEnabled(false);

    QTableView *tbl = new QTableView(ui->tabWidget);
    tbl->setModel(mdl);
    tbl->resizeColumnsToContents();

    ui->tabWidget->addTab(tbl,QString("&%1").arg(mdl->Label()));
    ui->tabWidget->setUpdatesEnabled(true);
}
