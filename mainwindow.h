#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>


#include "dbmodelextra.h"
#include <QList>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_action_Quit_triggered();
    void addDataTab(DbModelExtra *mdl);

private:
    Ui::MainWindow *ui;
    QList<DbModelExtra*> *dbm;

};

#endif // MAINWINDOW_H
