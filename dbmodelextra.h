#ifndef DBMODELEXTRA_H
#define DBMODELEXTRA_H


#include <QSqlQueryModel>
#include <QSqlDatabase>

class DbModelExtra : public QSqlQueryModel
{
    Q_OBJECT
    Q_PROPERTY(QString Label READ Label WRITE setLabel NOTIFY LabelChanged)
public:
    explicit DbModelExtra(QString dbname, QObject *parent = 0);
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &item, int role) const;
    
    QString Label() const
    {
        return m_Label;
    }

signals:
    
    void LabelChanged(QString arg);

public slots:

void setLabel(QString arg)
{
    if (m_Label != arg) {
        m_Label = arg;
        emit LabelChanged(arg);
    }
}

private:
    QSqlDatabase MemoryDB;

    bool initializeMemoryDB(QString dbname);
    bool createMemoryTable(QString dbname);
    bool populateMemoryTables(QString dbname);

    
    QString m_Label;
};

#endif // DBMODELEXTRA_H
